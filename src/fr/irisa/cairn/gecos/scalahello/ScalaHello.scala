package fr.irisa.cairn.gecos.scalahello;

import gecos.gecosproject.GecosProject;
import java.util.ArrayList;
import fr.irisa.cairn.tools.ecore.query.EMFUtils;
import fr.irisa.r2d2.gecos.framework.GSModule;

@GSModule("FIXME ScalaHello.")
class ScalaHello(p : GecosProject) {
  //val p : GecosProject;
  
  def VERBOSE = true;

	def debug(string : String) {
		if (VERBOSE) {
			println("[ScalaHello] "+string)

		}
	}
	
	def compute() {
		println("Hello from Scala in Gecos!");
	}
}
